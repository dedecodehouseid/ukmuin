$(document).ready(function(){
    $.ajax({
        url: site_url+"/home/data_pendaftaran",
        type: "GET",
        dataType: "json",
        success: function (data) {
            var nim = data.map(function(json){
                var nimk=json.nim.toUpperCase();
                return nimk;
            });
            // console.log(data);
            $("#nim_ketua_ukm").autocomplete({
                minLength: 5,
                source: nim,
                select: function(e, u) {
                    var valueNim = u.item.label;
                    var pendafTarget = data.find(function(item) {
                        return item.nim === valueNim;
                    })
                    $('#nim_ketua_ukm').val(valueNim);
                    $('#nama_ketua_ukm').val(pendafTarget.nama);
                }
            });

        }
    });
});