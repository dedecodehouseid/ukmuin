<div class="wrap_sidebar">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
            <img src="<?php echo base_url() ?>/assets/images/logo_uin.png" alt="" class="logo">
            </div>

            <ul class="list-unstyled components">
                <p><?php //echo $userdata['NAMA_KETUA_UKM'] ?></p>
                <li class="active">
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">UKM</a>
                    <ul class="collapse list-unstyled" id="homeSubmenu">
                        <li>
                            <a href="<?php echo site_url() ?>/home/update_profil_ukm">Update UKM</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url() ?>/home/berita">Berita</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url() ?>/home/tabel_anggota">Tabel Anggota</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url() ?>/home/tabel_pendaftaran">Tabel Pendaftaran</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url() ?>/home/atur_pendaftaran">Pengaturan Pendaftaran</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url() ?>/home/logout">Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    

    