<div id="content">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="navbar-btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>
                </div>
            </nav>
            <h2>Berita</h2>

            <form action="<?php echo site_url() ?>/home/tambah_berita/<?php echo $this->session->userdata('id'); ?>" class="form_tambah_ukm" method="post" enctype="multipart/form-data">
  <div class="form-group">
    <h3>Judul</h3>
    <input class="form-control form-control-lg" id="judul_berita" name="judul_berita" type="text" placeholder="Judul">
  </div>
  <div class="form-group">
    <label>Isi Berita</label>
    <textarea class="form-control" id="isi_berita_textarea" name="isi_berita_textarea" rows="6"></textarea>
  </div>
  <div class="form-group">
    <label>Gambar</label>
    <input type="file" class="form-control-file" id="file_gambar_berita" name="file_gambar_berita" accept="image/*">
    <img id="prev_foto_berita" src="" class="img-responsive img-thumbnail" alt="Preview Image" style="width:100%;">
  </div>
  <div class="form-group">
    <label>File</label>
    <input type="file" class="form-control-file" id="file_file_berita" name="file_file_berita">
  </div>
  <!-- <div id="drop_gambar_berita" name="drop_gambar_berita" class="drop_gambar">
      <input type="file" class="form-control" id="file_gambar_berita" accept="image/*">
      <img id="prev_foto_berita" src="" class="img-responsive img-thumbnail" alt="Preview Image" style="width:100%;">
  </div> -->
  <button type="submit" class="btn btn-primary" value="upload">Submit</button>
</form>


<table class="table" id="tabel_berita">
  <caption>List Berita</caption>
  <thead>
    <tr>
      <th scope="col">Judul Berita</th>
      <th scope="col">Tanggal Berita</th>
      <th scope="col">Opsi</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($berita as $data): ?>
    <tr>
      <td><?php echo $data->JUDUL_BERITA ?></td>
      <td><?php echo $data->TANGGAL_BERITA ?></td>
      <td><!--<button type="button" class="btn btn-success">Update Berita</button>--><a href="<?php echo site_url() ?>/home/hapus_berita/<?php echo $data->ID_BERITA ?>"><button type="button" class="btn btn-primary">Hapus Berita</button></a></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

            </div>

            <script>
function readURL(input) {
   if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
     $('#prev_foto_berita').attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
   }
  //  else if(input.files && input.files[1]){
  //   var reader = new FileReader();
  //   reader.onload = function (e) {
  //    $('#prev_foto1').attr('src', e.target.result);
  //   }
  //   reader.readAsDataURL(input.files[1]);
  //  }else if(input.files && input.files[2]){
  //   var reader = new FileReader();
  //   reader.onload = function (e) {
  //    $('#prev_foto1').attr('src', e.target.result);
  //   }
  //   reader.readAsDataURL(input.files[2]);
  //  }
  }

  $(document).ready(function(){
     $('#file_gambar_berita').change(function(){
       readURL(this);
     });
    //  $('#file_gambar2').change(function(){
    //    readURL(this);
    //  });
    //  $('#file_gambar3').change(function(){
    //    readURL(this);
    //  });
   });
   </script>