<div id="content">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
              <div class="container-fluid">
                    <button type="button" id="sidebarCollapse" class="navbar-btn">
                      <span></span>
                      <span></span>
                      <span></span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <i class="fas fa-align-justify"></i>
                </button>
              </div>
            </nav>
            <h2>Update Profil UKM</h2>
            <?php foreach($ukm as $data): ?>
            <form action="<?php echo site_url() ?>/home/update_profil/<?php echo $data->ID_UKM ?>" class="form_update_ukm" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label>Profile UKM</label>
                <textarea class="form-control" id="profile_ukm_baru" name="profile_ukm_baru" rows="6"><?php echo $data->PROFIL_UKM ?></textarea>
              </div>
              <div class="form-group">
                <div class="z-depth-1-half mb-4">
                  <!-- <img src="https://mdbootstrap.com/img/Photos/Others/placeholder.jpg" class="img-fluid" alt="example placeholder"> -->
                </div>
                <label>Gambar UKM</label>
                <!-- <input type="file" class="form-control-file" id="input_gambar_berita" name="input_gambar_berita"> -->
                <br/>
                <img id="tes_gambar_prev" src="<?php echo base_url() ?>assets/uploads/<?php echo $data->FOTO_PROFIL_UKM ?>" class="img-responsive img-thumbnail" alt="Preview Image" style="width:100%;">
                <input type="file" class="form-control" id="tes_gambar" name="tes_gambar" accept="image/*">
              </div>
              <button type="submit" class="btn btn-primary" value="upload">Submit</button>
            </form>
            <?php endforeach; ?>
            <br/>
            <br/>
            <h2>Foto Dokumentasi</h2>
            
            <form action="<?php echo site_url() ?>/home/update_dokumentasi/<?php echo $data->ID_UKM ?>" method="post" enctype="multipart/form-data">
            <div class="form-group">
                  <div id="drop_gambar1" name="drop_gambar1" class="drop_gambar">
                    <img id="prev_foto1" src="" class="img-responsive img-thumbnail" alt="Preview Image" style="width:100%;">
                    <input type="file" class="form-control" id="file_gambar" name="upload_dokumentasi1" accept="image/*">
                  </div>
            </div>
              <br/><br/><br/><br/><br/>
              
              
              
              
              <button type="submit" class="btn btn-primary" value="upload">Submit</button>
            </form>
            <br/>
            <table class="table">
  <caption>List UKM</caption>
  <thead>
    <tr>
      <!-- <th scope="col">No.</th> -->
      <th scope="col">Gambar</th>
      <th scope="col">Opsi</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($dokumentasi as $data): ?>
    <tr>
      <td><img id="foto" src="<?php echo base_url() ?>assets/dokumentasi/<?php echo $data->UPLOAD_GAMBAR1 ?>" class="img-responsive img-thumbnail" style="width:500px" alt="Preview Image" style="width:100%;"></td>
      <td><a href="<?php echo site_url() ?>/home/hapus_gambar_dokumentasi/<?php echo $data->ID_UKM ?>"><button type="button" class="btn btn-primary">Hapus</button></a>
      </td>
    </tr>
<?php endforeach; ?>
  </tbody>
</table>
  <!-- </div> -->
</div>

<script>
// function readURL(input) {
//    if (input.files && input.files[0]) {
//     var reader = new FileReader();
//     reader.onload = function (e) {
//      $('#tes_gambar_prev').attr('src', e.target.result);
//     }
//     reader.readAsDataURL(input.files[0]);
//    }
//   //  else if(input.files && input.files[1]){
//   //   var reader = new FileReader();
//   //   reader.onload = function (e) {
//   //    $('#prev_foto1').attr('src', e.target.result);
//   //   }
//   //   reader.readAsDataURL(input.files[1]);
//   //  }else if(input.files && input.files[2]){
//   //   var reader = new FileReader();
//   //   reader.onload = function (e) {
//   //    $('#prev_foto1').attr('src', e.target.result);
//   //   }
//   //   reader.readAsDataURL(input.files[2]);
//   //  }
//   }

  $(document).ready(function(){
     $('#tes_gambar').change(function readURL(input){
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        $('#tes_gambar_prev').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
       readURL(this);
     });
     $('#file_gambar').change(function readURL(input){
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        $('#prev_foto1').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
       readURL(this);
     });
     $('#file_gambar2').change(function readURL(input){
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        $('#prev_foto2').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
       readURL(this);
     });
     $('#file_gambar3').change(function readURL(input){
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        $('#prev_foto3').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
       readURL(this);
     });
    //  $('#file_gambar2').change(function(){
    //    readURL(this);
    //  });
    //  $('#file_gambar3').change(function(){
    //    readURL(this);
    //  });
   });
   </script>
