<div id="content">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="navbar-btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>
                </div>
            </nav>
            <h2>Tabel UKM</h2>
            <table class="table">
  <caption>List UKM</caption>
  <thead>
    <tr>
      <th scope="col">No.</th>
      <th scope="col">Nama UKM</th>
      <th scope="col">Status</th>
      <th scope="col">Opsi</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($ukm as $data): ?>
    <tr>
      <th scope="row"><?php echo $data->ID_UKM ?></th>
      <td><?php echo $data->NAMA_UKM ?></td>
      <td><?php echo $data->STATUS_UKM ?></td>
      <?php if($data->ID_STATUS_UKM=='1'): ?>
      <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#update<?php echo $data->ID_UKM ?>">Update</button>
      <a href="<?php echo site_url() ?>/home/update_status_beku/<?php echo $data->ID_UKM ?>"><button type="button" class="btn btn-primary">Bekukan</button></a>
      <button type="button" class="btn btn-primary" id="details" data-toggle="modal" data-target="#det<?php echo $data->ID_UKM ?>">Detail</button>
      </td>
      <?php elseif($data->ID_STATUS_UKM=='2'): ?>
      <td><a href="<?php echo site_url() ?>/home/update_status_unfreeze/<?php echo $data->ID_UKM ?>"><button type="button" class="btn btn-primary">Aktifkan</button></a>
      <button type="button" class="btn btn-primary" id="details" data-toggle="modal" data-target="#det<?php echo $data->ID_UKM ?>">Detail</button>
      </td>
      <?php elseif($data->ID_STATUS_UKM=='3'): ?>
      <td><a href="<?php echo site_url() ?>/home/update_status_unfreeze/<?php echo $data->ID_UKM ?>"><button type="button" class="btn btn-primary">Diterima</button></a>
      <button type="button" class="btn btn-primary" id="details" data-toggle="modal" data-target="#det<?php echo $data->ID_UKM ?>">Detail</button>
      </td>
<?php endif; ?>
    </tr>
<?php endforeach; ?>
  </tbody>
</table>
</div>

<?php foreach($ukm as $data): ?>
<div class="modal fade" id="det<?php echo $data->ID_UKM ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pendaftaran UKM <?php echo $data->NAMA_UKM ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <table class="table">
  <caption>List Anggota</caption>
  <thead>
    <tr>
      <th scope="col">Nama</th>
      <th scope="col">Nim</th>
      <th scope="col">Status</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($anggota as $data): ?>
    <tr>
      <td><?php echo $data->NAMA_ANGGOTA ?></td>
      <td><?php echo $data->NIM_ANGGOTA ?></td>
      <td><?php echo $data->STATUS_USER ?></td>
    </tr>
<?php endforeach; ?>
  </tbody>
</table>
      </div>
    </div>
  </div>
</div>
<?php endforeach; ?>


<?php foreach($ukm as $data): ?>
<div class="modal fade" id="update<?php echo $data->ID_UKM ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Update UKM</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="<?php echo site_url() ?>/home/update_ketua_ukm/<?php echo $data->ID_UKM ?>" class="form_tambah_ukm" method="post">
  <div class="form-group">
    <label>Nama UKM</label>
    <input type="text" class="form-control" id="nama_ukm_baru" placeholder="<?php echo $data->NAMA_UKM ?>" readonly>
  </div>
  <div class="form-group">
    <label>NIM Ketua UKM</label>
    <input type="text" class="form-control" id="nim_ketua_ukm" name="nim_ketua_ukm" placeholder="<?php echo $data->NIM_ADMIN ?>">
  </div>
  <div class="form-group">
    <label>Nama Ketua UKM</label>
    <input type="text" class="form-control" id="nama_ketua_ukm" name="nama_ketua_ukm" placeholder="<?php echo $data->NAMA_ADMIN ?>" readonly>
  </div>
  <button type="submit" class="btn btn-primary" id="update_ukm">Update</button>
</form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
<?php endforeach; ?>