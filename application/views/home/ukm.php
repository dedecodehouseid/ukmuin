<div class="ukm_wrap" id="ukm_wrap">
<div class="judul_tabel_ukm">
  <h2>Info UKM</h2>
</div>
<div class="search_filter_ukm">
  <input class="form-control form-control-sm" type="text" placeholder="Cari UKM" id="filter_ukm" onkeyup="filter_ukm_function()">
</div>
<table class="table_ukm">
  <caption>List UKM</caption>
  <thead class="head_table">
    <tr>
      <th>No.</th>
      <th>Nama UKM</th>
      <th>Opsi</th>
    </tr>
  </thead>
  <tbody class="body_table">
  <?php foreach ($ukm as $data): ?>
    <tr class="baris_ukm">
      <td><?php echo $data->ID_UKM ?></td>
      <td><?php echo $data->NAMA_UKM ?></td>
      <td><button type="button" class="btn btn-success" id="tombol_daftar" data-toggle="modal" data-target="#ex<?php echo $data->ID_UKM ?>">Daftar</button><a href="<?php echo site_url() ?>/home/detail_ukm/<?php echo $data->ID_UKM ?>"><button type="button" class="btn btn-success" id="tombol_detail">Detail</button></a></td>
    </tr>
  <?php endforeach; ?>
    <!-- <tr class="baris_ukm">
      <td>2</td>
      <td>IQMA</td>
      <td><button type="button" class="btn btn-success" id="tombol_detail">Detail</button></td>
    </tr>
    <tr class="baris_ukm">
      <td>3</td>
      <td>PRAMUKA</td>
      <td><button type="button" class="btn btn-success" id="tombol_detail">Detail</button></td>
    </tr> -->
  </tbody>
</table>
</div>
<!-- Modal -->
<?php foreach($ukm as $data): ?>
<div class="modal fade" id="ex<?php echo $data->ID_UKM ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pendaftaran UKM <?php echo $data->NAMA_UKM ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="<?php echo site_url() ?>/home/simpan_anggota/<?php echo $data->ID_UKM ?>" method="post">
        <div class="form-group">
          <label>NIM</label>
          <input type="text" class="form-control" id="input_nim" name="input_nim" placeholder="NIM">
        </div>
        <div class="form-group">
          <label>Nama</label>
          <input type="text" class="form-control" id="disabled_nama" name="disabled_nama" placeholder="Nama" readonly>
        </div>
        <div class="form-group">
          <label>Semester</label>
          <input type="text" class="form-control" id="disabled_semester" name="disabled_semester" placeholder="Semester" readonly>
        </div>
        <div class="form-group">
          <label>Tanggal Lahir</label>
          <input type="text" class="form-control" id="disabled_tanggal" name="disabled_tanggal" placeholder="Tanggal Lahir" readonly>
        </div>
        <div class="form-group">
          <label >Tempat Lahir</label>
          <input type="text" class="form-control" id="disabled_tempat" name="disabled_tempat" placeholder="Tempat Lahir" readonly>
        </div>
        <div class="form-group">
          <label >Fakultas</label>
          <input type="text" class="form-control" id="disabled_fakultas" name="disabled_fakultas" placeholder="Fakultas" readonly>
          <!-- <select class="form-control" id="disabled_fakultas" disabled>
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
          </select> -->
        </div>
        <div class="form-group">
          <label >Prodi</label>
          <input type="text" class="form-control" id="disabled_prodi" name="disabled_prodi" placeholder="Prodi" readonly>
          <!-- <select class="form-control" id="disabled_prodi" disabled>
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
          </select> -->
        </div>
        <div class="form-group">
          <label >Alamat Tinggal</label>
          <textarea class="form-control" id="alamat_tinggal_anggota" name="alamat_tinggal_anggota" rows="3"></textarea>
        </div>
        <div class="form-group">
          <label >Nomor Telepon</label>
          <input type="text" class="form-control" id="nomor_telepon_anggota" name="nomor_telepon_anggota" placeholder="Nomor Telepon">
        </div>
        <div class="form-group">
          <label >Deskripsi Diri</label>
          <textarea class="form-control" id="deskripsi_diri_anggota" name="deskripsi_diri_anggota" rows="6"><?php echo $data->DESKRIPSI_PENDAFTAR ?></textarea>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="daftar_ukm">Daftar</button>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
<?php endforeach; ?><!-- END MODAL -->
<!-- <script type="text/javascript">
        $("#nomor_telepon_anggota").keydown(function (e) {
			// Allow: backspace, delete, tab, escape, enter and .
			if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
				// Allow: Ctrl/cmd+A
				(e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
				// Allow: Ctrl/cmd+C
				(e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
				// Allow: Ctrl/cmd+X
				(e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
				// Allow: home, end, left, right
				(e.keyCode >= 35 && e.keyCode <= 39)) {
				// let it happen, don't do anything
				return;
			}
			// Ensure that it is a number and stop the keypress
			if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
			}
		});
    </script> -->