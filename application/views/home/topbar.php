<header class="header d-flex flex-row scrolled">
  <div class="header_content d-flex flex-row align-items-center"> 

    <!-- Logo -->
    <div class="logo_container">
      <a href="<?php echo base_url() ?>"><img src="<?php echo base_url() ?>/assets/images/logo_uin.png" alt="" class="logo"></a>
    </div>

    <!-- Main Navigation -->
    <nav class="main_nav_container">
      <div class="main_nav">
        <ul class="main_nav_list">
          <li class="main_nav_item"><a href="<?php echo base_url() ?>">beranda</a></li>
          <li class="main_nav_item"><a href="#ukm_wrap">ukm</a></li>
          <!-- <li class="main_nav_item"><a href="#">beasiswa</a></li> -->
        </ul>
      </div>
    </nav>
  </div>
  <div class="header_side d-flex flex-row justify-content-center align-items-center">
    <ul>
      <li class="main_nav_item_side"><a href="#login">login</a></li>
    </ul>
  </div>

  <!-- Hamburger -->
  <!-- <div class="searchicon_container">
    <i class="fa fa-search trans_200" title="Cari Alumni"></i>
  </div>
  <div class="hamburger_container">
    <i class="fa fa-bars trans_200"></i>
  </div> -->
</header>