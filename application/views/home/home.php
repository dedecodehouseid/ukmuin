<div class="row row-eq-height">
      <div class="col-lg-6 nopadding">

        <!-- login -->

        <div class="login_section d-flex flex-column align-items-center justify-content-center">
          <div class="login_content text-center">
              <h1 class="login_title">"When I consider <span>what people generally want</span>  in calculating, <span> I found that</span> it always is a number."</h1>
            <p class="login_text">-- Muhammad bin Musa al-Khawarizmi  --</p>
          </div>
        </div>

      </div>

      <div class="col-lg-6 nopadding">

        <!-- Search -->
        
        <div class="search_section d-flex flex-column align-items-center justify-content-center" id="login">
          <div class="search_content text-center">
            <h1 class="search_title">Login</h1>
            <form id="login_form" class="search_form" action="<?php echo site_url(); ?>/home/login" method="post" role="login">
              <input id="nim" class="input_field" type="text" placeholder="NIM/NIP" name="nim" required="required">
              <input id="passwordlogin" class="input_field" type="password" placeholder="Password" name="passwordlogin">
              <button id="login_id" type="submit" class="search_submit_button trans_200" value="Submit">login</button>
            </form>
          </div>
        </div>

      </div>
    </div>                                